#include <stdio.h>
#include <stdlib.h>
#include <math.h>
float arb_func (float x);
void rf (float *x, float x0, float x1, float fx0, float fx1, int *itr, FILE *outp);


int main()
{

    FILE *outp;

    int itr = 0;
    int maxitr;
    float x0;
    float x1;
    float x_curr;
    float x_next;


    float error;

    outp=fopen("rf.txt","w");


    printf("Please enter x0:" );
    scanf("%lf",&x0);
    printf("Please enter x1:" );
    scanf("%lf",&x1);
    printf("Please enter error:" );
    scanf("%lf",&error);
    printf("Please enter maxitr: " );
    scanf("%lf",&maxitr);


    rf(&x_curr, x0, x1, arb_func(x0), arb_func(x1), &itr, outp);
    do{
        if((arb_func(x0)*arb_func(x_curr))<0){
            x1 = x_curr;
        }
        else{
            x0 = x_curr;
        }

        rf(&x_next , x0, x1, arb_func(x0), arb_func(x1), &itr, outp);
        if(fabs(x_next - x_curr) < error){
            printf("After %d iteration root is %lf\n",itr,x_next);
            fprintf(outp,"After %d iteration root is %lf\n",itr,x_next);
            return 0;
        }
        else{
            x_next = x_curr;
        }
    }

    while(itr < maxitr);
    printf("You cannot converge\n");
    fprintf(outp, "You cannot converge\n");

    fclose(outp);
    return(1);
}


float arb_func (float x)
{
    return x*log10(x)-1.2;
}

void rf (float *x, float x0, float x1, float fx0, float fx1, int *itr, FILE *outp)
{
    *x = ((x0*fx1)-(x1*fx0))/(fx1-fx0);
    ++(*itr);
    printf("Iteration %d: %.5f\n",*itr,*x);
    fprintf(outp ,"Iteration %d: %.5f\n",*itr,*x);

}
