#include <stdio.h>
#include <stdlib.h>
#include <math.h>
void printArray(const int arr[], const int size); // Prints the array with size, size
void swap(int* p, int* k); // Swaps the values pointed by p and k
void stoogesort(int arr[], const int low, const int high); // Recursive function, low and high are array indices



int main()
{
    int i;
    static int size;
    printf("Enter the size of array:");
    scanf("%d",&size);
    int arr[size];
    printf("Enter the member of array with pressing enter:");
    for(i = 0;i<size;i++){
        scanf("%d",&arr[i]);
    }
    printf("Before the sort:\n");
    printArray(arr,size);
    printf("\n After the sort:");
    stoogeSort(arr,0,size-1);
    printArray(arr,size);
    return 0;
}



void printArray(const int arr[],const int size){
    int i ;
    for (i=0;i<size;i++){
        printf("\n%d. index= %d",i,arr[i]);
    }

}


void swap(int* p, int* k){
    int temp;
    temp = *p;
    *p = *k;
    *k = temp;
}



void stoogeSort(int arr[], const int low, const int high){
if(low>=high){
    return;
}if(arr[low]>arr[high]){
    swap(&arr[low],&arr[high]);
}
if(high-low+1>2){
        int size = high-low+1;
        stoogeSort(arr,low,high-floor(size/3));     // Here we are checking first 2/3rd arrays members
        stoogeSort(arr,low+floor(size/3),high);     // Here we are checking last 2/3rd arrays members
        stoogeSort(arr,low,high-floor(size/3));     // Here we are checking first 2/3rd arrays members

}

}




